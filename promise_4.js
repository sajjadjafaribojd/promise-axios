/* jshint esversion:6 */

const request=require('request');

var geocodeAddress=(address)=>{
    return new Promise((resolve,reject)=>{
        var urlEncodeAddress=encodeURIComponent(address);

        request({
            url:`http://open.mapquestapi.com/geocoding/v1/address?key=HJdUJzSFNtpi1WNFvtqwccX8l61KyLc1&location=${urlEncodeAddress}`,
            json:true
        },(error,response,body)=>{
            if(error){
                reject('Unable to connect server.');
            }else if(body.status===1){
                reject('Unable to find address.');
            }else{
                resolve({
                    address:body.results[0].locations[0].adminArea3,
                    Latitude:body.results[0].locations[0].latLng.lat,
                    Longitude:body.results[0].locations[0].latLng.lng
                });
            }
        });
    });
};

geocodeAddress('tehran').then((result)=>{
    console.log(JSON.stringify(result));
},(errorMessage)=>{
    console.log(errorMessage);
});