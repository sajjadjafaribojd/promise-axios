/* jshint esversion:6 */

var asyncAdd=(a,b)=>{
    return new Promise((resolve,reject)=>{
        if(typeof a ==='number' && typeof b === 'number'){
            resolve(a+b);
        }else{
            reject('Arguments must be number');
        }
    });
};

asyncAdd(2,6).then((result)=>{
    console.log('Result:',result);
    return asyncAdd(result,10);
}).then((result)=>{
    console.log('Second proocess result is:',result);
}).catch((errorMessage)=>{  // creat catch instead of error message function; show promise_2.js for watch diffrent.
    console.log(errorMessage);
});


