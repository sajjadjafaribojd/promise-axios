/* jshint esversion:6 */

var asyncAdd=(a,b)=>{
    return new Promise((resolve,reject)=>{
        if(typeof a ==='number' && typeof b === 'number'){
            resolve(a+b);
        }else{
            reject('Arguments must be number');
        }
    });
};

asyncAdd(2,6).then((result)=>{
    console.log('Result:',result);
    return asyncAdd(result,10);
},(errorMessage)=>{
    console.log(errorMessage);
}).then((result)=>{
    console.log('Second proocess result is:',result);
},(errorMessage)=>{
    console.log(errorMessage);
});


