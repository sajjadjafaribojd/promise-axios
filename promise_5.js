/* jshint esversion:6 */

const yargs=require('yargs');
const axios=require('axios');


var argv=yargs.option({
    a:{
        alias:'address',
        demand:true,
        string:true,
        describe:'Address to fetch weather for'
    }
})
.help()
.alias('help','h')
.argv;

var urlEncodeAddress=encodeURIComponent(argv.address);
var geocodeUrl=`http://open.mapquestapi.com/geocoding/v1/address?key=HJdUJzSFNtpi1WNFvtqwccX8l61KyLc1&location=${urlEncodeAddress}`;

axios.get(geocodeUrl).then((response)=>{
  // console.log(response.data.results[0].locations[0].adminArea3);
  var locationAddress=response.data.results[0].locations[0].adminArea3;
  if(response.data.status===0){
      throw new Error('Unable to find address'); // ایجاد یک مدیریت خطا  - با این کار دیگه پایین تر نمیره
  }

  var lat=response.data.results[0].locations[0].latLng.lat;
  var lng=response.data.results[0].locations[0].latLng.lng;
  var weatherUrl=`https://api.darksky.net/forecast/9ca899962e04021578f0c569919e163b/${lat},${lng}`;

  axios.get(weatherUrl).then((response)=>{
    console.log(`${locationAddress} temp is ${response.data.currently.temperature}`);
  });
  
}).catch((e)=>{ //error handel.
    if(e.code===0){
        console.log('Unable to connect to API server');
    }else{
        console.log(e.message);
    }
});