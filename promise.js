/* jshint esversion:6 */

var somePromise=new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve('Hi this is a test');
        //resolve('message reuslt 2');
        //reject('Unable to fulfill promise');
    }, 2500);
   
});

somePromise.then((message)=>{
    console.log('Success',message);
},(errorMessage)=>{
    console.log('Error:',errorMessage);
});

console.log('this message print befor promise module');